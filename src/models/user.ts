import { USER_TYPE } from '../environments/environment';

export class User {
  uid: String;
  displayName: String;
  photoURL: String;
  email: String;
  address: string;
  phone: string;
  city: string;
  type: USER_TYPE;
  fullDetails: boolean;
}
