// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD3yV0X_xeKOj3xhH7lTEusaTKmN7yhXKM',
    authDomain: 'dotolist-3ecbe.firebaseapp.com',
    databaseURL: 'https://dotolist-3ecbe.firebaseio.com',
    projectId: 'dotolist-3ecbe',
    storageBucket: 'dotolist-3ecbe.appspot.com',
    messagingSenderId: '578843802433'
  }
};

export const enum USER_TYPE {
  SUPER_USER,
  ADMIN,
  USER
}
