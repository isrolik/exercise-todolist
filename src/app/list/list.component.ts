import { SlicePipe } from '@angular/common/src/pipes/slice_pipe';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
// import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {
  list: any[] = [];
  @ViewChild('newNote') add;
  cantEdit = true;
  // listForm: FormGroup;

  constructor() {}
  // constructor(private fb: FormBuilder) {
  //   this.createForm();
  //  }

  //  createForm() {
  //   this.listForm = this.fb.group({
  //     name: ['', Validators.required ],
  //     }
// }

  ngOnInit() {
    if (localStorage.list) {
      this.list = JSON.parse(localStorage.list);
    }
  }

  enter(e) {
    if (e.keyCode === 13) {
      this.addButton();
    }
  }

  addButton(): void {
    this.list.unshift({text: this.add.nativeElement.value, done: false});
    this.add.nativeElement.value = '';
    this.saveToLocal();
  }

  editButton(): void {
    this.cantEdit = false;
  }

  saveButton(): void {
    this.cantEdit = true;
  }

  doneButton(item): void {
    const index = this.list.indexOf(item);
    if (this.list[index].done) {
      this.list[index].done = false;
    } else {
      this.list[index].done = true;
    }
    this.saveToLocal();
  }
  deleteButton(item): void {
    const index = this.list.indexOf(item);
    this.list.splice(index, 1);
    this.saveToLocal();
  }

  saveToLocal() {
    localStorage.list = JSON.stringify(this.list);
  }
}
