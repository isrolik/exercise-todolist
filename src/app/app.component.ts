import { Component, ViewChild, OnInit } from '@angular/core';
import { User } from '../models/user';
import { USER_TYPE } from '../environments/environment';

import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FormBuilder, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  items: Observable<any[]>;
  options: FormGroup;
  showSideNav = false;
  @ViewChild('sidenav') sideNav;

  constructor(
    public Auth: AngularFireAuth,
    private db: AngularFirestore,
    fb: FormBuilder
  ) {
    this.options = fb.group({
      'fixed': true,
      'top': 30,
      'bottom': 30,
    });
    // this.items = db.collection('items').valueChanges();
  }

  ngOnInit() {
    this.Auth.authState.subscribe(result => {
      if (result === null) {
        this.showSideNav = true;
      }
    });
  }
  login() {
    this.Auth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then(result => {
      console.log(result);
    })
    .catch(err => console.log(err));
    setTimeout(() =>  {this.openSideNav(); }, 10000);
  }

  logout() {this.Auth.auth.signOut(); }

  openSideNav() {
      if (!this.showSideNav) {
        this.showSideNav = true;
        setTimeout(() =>  {this.sideNav.toggle(); }, 200);
      } else {
        this.sideNav.toggle();
        setTimeout(() => {this.showSideNav = false; }, 200);
      }
  }
}
